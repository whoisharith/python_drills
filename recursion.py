from pprint import pprint

def get_dict_children(json_dict, res_list, attr_type, attr_val):
    
    children = json_dict["children"]
    
    if attr_type in json_dict["attrs"].keys() and json_dict["attrs"][attr_type] == attr_val:
        res_list.append(json_dict)

    for child in children:
        get_dict_children(child, res_list, attr_type, attr_val)


def html_dict_search(html_dict, selector):
    """
    Implement `id` and `class` selectors
    """
    if selector[0]==".":
        attr_type = "class"
    elif selector[0]=="#":
        attr_type = "id"
    attr_val = selector[1:]

    children = html_dict["children"]
    res_list = []
    for child in children:
        get_dict_children(child, res_list, attr_type, attr_val)

    return res_list
