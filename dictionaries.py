from re import sub

def word_count(s):
    split_sentence = s.split(' ')
    word_count_dict = {}
    for word in split_sentence:
        word = sub('[\,\.]','',word)
        if word in word_count_dict:
            word_count_dict[word] += 1
        else:
            word_count_dict[word] = 1

    return word_count_dict
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    


def dict_items(d):
    answer = []
    for letter in d:
        answer.append( (letter, d[letter]) )
    return answer
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    pass

def getLetter(letter_number_pair):
    return letter_number_pair[0]

def dict_items_sorted(d):
    answer = dict_items(d)
    answer.sort(key=getLetter)
    return answer
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    pass
