"""
* Create a class called `Point` which has two instance variables,
`x` and `y` that represent the `x` and `y` co-ordinates respectively. 

* Initialize these instance variables in the `__init__` method

* Define a method, `distance` on `Point` which accepts another `Point` object as an argument and 
returns the distance between the two points.
"""


class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y


    def distance(self, other_point):
        pointX = other_point.x
        pointY = other_point.y
        return ( (self.x-pointX)**2 + (self.y-pointY)**2 )**0.5
