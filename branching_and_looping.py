from math import ceil

def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    numbers = []
    for num in range(start, end, step):
        numbers.append(num)
    return numbers


def integers_from_start_to_end_using_while(start, end, step):
    numbers = []
    num = start

    while 0<ceil( (end-num)/step ):
        numbers.append(num)
        num += step

    return numbers


def isPrime(num):
    for i in range(2,int(num**0.5)+1):
        print(i, num, int(num**0.5))
        if(num%i==0):
            return False
    return True

def two_digit_primes():
    primes = []
    for number in range(11, 100, 2):
        if isPrime(number):
            primes.append(number)
    return primes
