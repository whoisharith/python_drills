def is_prime(n):
    if n == 1:
        return False
    for divisor in range(2, int(n**0.5)+1):
        if n%divisor == 0:
            return False
    return True
    """
    Check whether a number is prime or not
    """


def n_digit_primes(digit=2):
    lower_limit = 1 * 10**(digit-1)
    upper_limit = lower_limit * 10
    primes = []
    for number in range(lower_limit, upper_limit):
        if is_prime(number):
            primes.append(number)
    return primes
    
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
