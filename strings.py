def last_3_characters(x):
    if(len(x)==0):
        return x
    return x[-3:]


def first_10_characters(x):
    if(len(x)<=10):
        return x
    return x[:10]


def chars_4_through_10(x):
    if(len(x)==0):
        return x
    return x[4:11]


def str_length(x):
    return len(x)


def words(x):
    if len(x)==0:
        return []
    return x.split(' ')


def capitalize(x):
    return x.capitalize()


def to_uppercase(x):
    return x.upper()