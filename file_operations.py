"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    myfile = open(path,'r')
    text = myfile.read()
    myfile.close()
    return text


def write_to_file(path, s):
    myfile = open(path, 'w')
    myfile.write(s)
    myfile.close()
    return


def append_to_file(path, s):
    myfile = open(path, 'a+')
    myfile.write(s)
    myfile.seek(0)
    text = myfile.read()
    myfile.close()


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    myfile = open(file_path,'w')
    for num in range(1,n+1):
        myfile.write(str(num)+","+str(num**2)+"\n")
    myfile.close()
    return


# file_path = "/home/harith/MountBlue drills/sample.txt"
# csv_path = "/home/harith/MountBlue drills/squares.csv"
# input = "append this string"

# write_to_file(file_path,"abcd")
# append_to_file(file_path, input)
# #read_file(file_path)
# numbers_and_squares(9,csv_path)
# read_file(csv_path)
